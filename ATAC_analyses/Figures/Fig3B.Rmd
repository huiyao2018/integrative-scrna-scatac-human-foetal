---
title: "Figure 3B"
output: html_notebook
---

```{r}
library(Seurat)
library(tidyverse)
library(BSgenome.Hsapiens.UCSC.hg38)
library(EnsDb.Hsapiens.v86)
library(Signac)
library(cowplot)
library(patchwork)
library(forcats)
```

```{r}
# loading objects
test = readRDS("/datadisk/Desktop/Projects/SCRNATAC/scATAC/files/test.merged234.Snap.HA.ChromVar.Integr3kb.Rds")

```

# Main body 

```{r}
save = F
# This is taking the gene body to the account 
# size of extension upstream and downstream in kb 

extensions = c(0,1,2,3,4,5,10,15)
extensions = c(3)

# extract gene coordinates from Ensembl, and ensure name formatting is consistent with Seurat object 
gene.coords <- genes(EnsDb.Hsapiens.v86, filter = ~ gene_biotype == "protein_coding")
seqlevelsStyle(gene.coords) <- 'UCSC'
genebody.coords <- keepStandardChromosomes(gene.coords, pruning.mode = 'coarse')


for (ext in extensions) {
  
  
  
  genebodyandpromoter.coords <- Extend(x = gene.coords, upstream = 1000*ext, downstream = 1000*ext)
  
  
  if (file.exists(paste0("/datadisk/Desktop/Projects/SCRNATAC/scATAC/geneact/GENEact.",ext,
                         "kdownup.merged2l234.rds"))) {
    
    gene.activities = readRDS(file = paste0("/datadisk/Desktop/Projects/SCRNATAC/scATAC/geneact/GENEact.",ext,
                                            "kdownup.merged2l234.rds"))
  } else {
    gene.activities <- FeatureMatrix(
      fragments = "/datadisk/Desktop/Projects/SCRNATAC/scATAC/fragments/merged.l234.sort.CO.bed.gz",
      features = genebodyandpromoter.coords,
      chunk = 20,
      cells = rownames(test@meta.data)
    )
    saveRDS(gene.activities, file = paste0("/datadisk/Desktop/Projects/SCRNATAC/scATAC/geneact/GENEact.",ext,
                                           "kdownup.merged2l234.rds"))
    
    
    gene.activities = readRDS(file = paste0("/datadisk/Desktop/Projects/SCRNATAC/scATAC/geneact/GENEact.",ext,
                                            "kdownup.merged2l234.rds"))
    
    
  }
  
  
  gene.key <- genebodyandpromoter.coords$gene_name
  names(gene.key) <- GRangesToString(grange = genebodyandpromoter.coords)
  
  
  rownames(gene.activities) <- gene.key[rownames(gene.activities)]
  
  gene.activities = gene.activities[unique(rownames(gene.activities)),]
  
  # add the gene activity matrix to the Seurat object as a new assay, and normalize it
  test[[paste0("RNA_geneprom_",ext,"kb")]] <- CreateAssayObject(counts = gene.activities)
  # test = BinarizeCounts(object = test, assay = c("RNA"))
  
  test <- NormalizeData(
    object = test,
    assay = paste0("RNA_geneprom_",ext,"kb"),
    normalization.method = 'LogNormalize',
    scale.factor = median(test@meta.data[,paste0("nFeature_RNA_geneprom_",ext,"kb")])
  )
  
  rm(gene.activities)
  
  # Make dotplot for the GENEACCes around 3kb from the gene body 
  
  # here change the thing to make different assays 
  dotx = DotPlot(test, assay = paste0("RNA_geneprom_",ext,"kb"), 
                 features = rev(c("CD34","CD52","SELL","PROM1","MLLT3","GATA1","KLF1","CTNNBL1","TESPA1","TFRC","MPO","LYZ",
                                  "AZU1","SPI1","CLECL11A","ALAS2","HBG1","HBA1","GYPA","GATA2","HDC","CD63","ITGA2B","FLI1",
                                  "MPEG1","IL3RA","IRF8","ITGAM","CD14","CD33","CD79B","IGLL1","IGHM","CD19","CDH5","PECAM1",
                                  "CD3E","NCAM1"))) +
    RotatedAxis() + 
    scale_colour_gradient2(low = "darkred", mid = "white",high = "darkgreen",midpoint = 0)
  
  
  
  # Main plot
  
  
  minmaxthis = function(x) { 
    (x - min(x))/(max(x) - min(x))
  }
  
  data.dot = dotx$data
  
  levels(data.dot$id)[levels(data.dot$id)=="0"] <- "7"
  data.dot$id = fct_relevel(data.dot$id, as.character(seq(1:7)))
  
  
  scaled_data <- 
    data.dot %>%
    group_by(features.plot) %>%
    mutate(minmaxscale = minmaxthis(avg.exp))
  
  scaled_data$avg.exp2 = ( scaled_data$avg.exp - min(scaled_data$avg.exp) )    / 
    (max(scaled_data$avg.exp)- min(scaled_data$avg.exp))
  
  
  
  pmain <- ggplot(scaled_data)  + 
    geom_point(aes(x = features.plot, y = id, color = minmaxscale, size = pct.exp), stat = "identity") + 
    scale_size(range = c(0, 4.25), breaks  = seq(0,100,20), labels  = paste0(seq(0,100,20),"%"))  + 
    scale_color_gradient(low = "white",high = "#0f4d91",breaks = seq(0,1,0.2))  +
    guides(color = guide_colourbar(title = "Accessibility", barwidth = 1, barheight = 10),
           size = guide_legend(title = "Cells (%)",reverse=TRUE)) + 
    xlab("") + ylab("") + 
    
    theme(panel.border=element_blank(), 
          axis.line = element_line(colour = 'black', size = 1), 
          axis.ticks = element_blank(),
          axis.text = element_text(size=12, colour = "black"), 
          axis.text.x = element_text(angle = 90, hjust = 1),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          panel.background = element_blank(),
          legend.key=element_blank(),
          # legend.title=element_blank(),
          legend.text=element_text(size=12))
  
  # Marginal densities along x axis
  
  
  
  
  xdens <- ggplot() +
    geom_bar(data = scaled_data, aes(x = features.plot, y = avg.exp2),stat = "summary",
             alpha = 1, size = 0.2, fill = "black") + 
    scale_y_continuous(breaks = seq(0,1,0.5), labels = seq(0,1,0.5), limits = c(0,1)) + 
    
    # theme_bw() + 
    theme(panel.border=element_blank(), 
          axis.line = element_line(colour = 'black', size = 1), 
          axis.ticks = element_blank(),
          axis.line.x = element_blank(),
          axis.text = element_text(size=12, colour = "black"), 
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          panel.background = element_blank(),
          legend.key=element_blank(),
          # legend.title=element_blank(),
          legend.text=element_text(size=12),
          axis.text.x = element_blank(),
          axis.ticks.x = element_blank(),
          axis.title.x  = element_blank()) + 
    ylab("Average \n accessibility")
  
  dotx1 = xdens + pmain + plot_layout(ncol = 1, heights = c(1,3))
  
  
  if (save) {
    cairo_pdf(file = paste0("/datadisk/Desktop/Projects/SCRNATAC/scATAC/plots/final/ga/genebody/dotplot.",ext,"kbGeneBody.pdf"),
            height = 6, width = 8)
  
    print(dotx1)
    dev.off()
    
  } else {
    print(dotx1)
  }

  
}
```

